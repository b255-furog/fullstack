import React from 'react'

// Create a context object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other compnents within this app
// the context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext()

// The 'Provider' component allos other components to consume/use the context object and supply the necessary information needed to the context objext

export const UserProvider = UserContext.Provider;

export default UserContext;