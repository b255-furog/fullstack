import{useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'

export default function Logout(){
	// localStorage.clear()
	// Comsume the UserContext object and destructure it to access the user state and unsetUser function from the context provider

	const {unsetUser, setUser} = useContext(UserContext)

	// Clear the localStoarge of the Users inoformation

	unsetUser()

	// Placing the "setUser" setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render

	useEffect(()=>{
		// Set user state back to its original value
		setUser({id: null})
	})

	return(

		<Navigate to ='/login' />
		)
}