// import Button from 'react-bootstrap/Button';
// // Bootstrap grid system components
// import {Row} from 'react-bootstrap';
// import {Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom'
import {Button, Row, Col} from 'react-bootstrap'
import React from 'react'
import {Fragment} from 'react'
import NotFound from '../pages/NotFound'
 export default function Banner(){
 	return(
 		<Row>
 	        <Col className ="p-5">
		        <h1>Zuitt Coding Bootcamp</h1>
 		        <p>Opportunities for everyone, everywhere.</p>
 		        <Button variant="primary">Enroll now!</Button>
		    </Col> 		
 	    </Row>




	)	
 }

// export default function Banner({ isHomePage }) {
//   const buttonText = isHomePage ? 'Enroll now!' : 'Go back to homepage';

//   return (
//     <Row>
//       <Col className="p-5">
//         <h1>Zuitt Coding Bootcamp</h1>
//         <p>Opportunities for everyone, everywhere.</p>
//         <Button variant="primary">
//           {buttonText}
//         </Button>
//       </Col>
//     </Row>
//   );
// }
