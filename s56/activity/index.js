console.log("hello world")

function countLetter(letter, sentence) {
    let result = 0;

   if (typeof letter === 'string' && letter.length === 1) {
    // letter is a single character

    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }

    return result;
} else {
    // letter is not a single character or is not a string
    return undefined;
}
}


function isIsogram(text) {
    // Convert the input text to lowercase to disregard text casing
    const lowercaseText = text.toLowerCase();

    // Create an empty object to store letters as keys
    const letterCounts = {};

    // Loop through each character in the input text
    for (let i = 0; i < lowercaseText.length; i++) {
        const letter = lowercaseText[i];

        // Check if the current letter has already been seen
        if (letterCounts[letter]) {
            // If it has, return false because it's not an isogram
            return false;
        } else {
            // If it hasn't, add it to the letterCounts object
            letterCounts[letter] = true;
        }
    }

    // If we made it through the entire loop without returning false,
    // then the input text is an isogram, so we return true
    return true;
}



function purchase(age, price) {
        // Return undefined for people aged below 13.
  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21 || age >= 65) {
    return Math.round(price * 0.8).toString();
  } else {
    return Math.round(price).toString();
  }
}
    
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    

    function findHotCategories(items) {
  // Initialize an object to store categories as keys and their stock count as values
  const categories = {};
  
  // Iterate through the items array and update the stock count for each category
  for (const item of items) {
    if (item.stocks === 0) {
      if (categories[item.category]) {
        categories[item.category]++;
      } else {
        categories[item.category] = 1;
      }
    }
  }

  // Extract the categories with no stocks left
  const hotCategories = Object.keys(categories).filter(category => categories[category] > 0);
  
  // Return the hot categories
  return hotCategories;
}

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.


function findFlyingVoters(candidateA, candidateB) {

    // Use a Set to store the candidates who voted for candidateA
  const votedForA = new Set(candidateA);
  // Use a Set to store the candidates who voted for candidateB
  const votedForB = new Set(candidateB);
  // Use the Array.filter method to find the candidates who voted for both
  const bothVotedFor = candidateA.filter(candidate => votedForB.has(candidate));
  // Return the array of candidates who voted for both
  return bothVotedFor;
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

// module.exports = {
//     countLetter,
//     isIsogram,
//     purchase,
//     findHotCategories,
//     findFlyingVoters
// };