import {Button, Row, Col} from 'react-bootstrap'

export default function NotFound() {
  return (
    <Row>
      <Col className="p-5">
        <h3>Zuitt Coding</h3>
        <h1>Page not Found.</h1>
        <p>Sorry, the page you are looking for cannot be found.</p>
        <Button href="/" variant="primary">
          Go back to home
        </Button>
      </Col>
    </Row>
  );
}