
import {Card, Button} from 'react-bootstrap';
import{useState,useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
export default function CourseCard({courseProp}){
	// check to see if the data was successfully passed
	// console.log(props)
	// every component receives information in a form of an object
	// console.log(typeof props)

	// use the state hook for this component to be able to store its state 
	// State are used to keep track of the information related to individual components

	const [count,setCount] = useState(0)
	const [seat,setSeats] = useState(30)

	// using the state hook returns an array with the first element being a value and the second element as function that used to change the value of the first element
	console.log(useState(0))

	// Function that keeps track of the enrolles for a course
	// By default JavaScript is synchronous its executes code from the top of the file all the way down
	// The setter function for useStates are asynchronous allowing it to execute separately from other program
	// The 'setCount' function is being executed while the console.log is already completed
	/*function enroll(){
		setCount(count + 1)
		console.log('Enrollees' + count)
		setSeats(seat -1)
		console.log('Seat' + seat)
		
	}*/
	// Define a "useEffect" hook to have the "CourseCard" component perform a certain task after every DOM update
	// This is run automatically after initial render and for every DOM UPDATE
	// Checking for the availability for enrollment of a course is better suited here
	// React will re-run this effect ONLY if any of the values contained in this array has changed from the last render/update
	/*useEffect(()=>{
		if(seat ===0){
			// setIsOpen(false)
		}
	},[seat])
		*/

	const{name, description,price, _id}=courseProp
	return(
		
			 <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP{price}</Card.Text>
                <Card.Text>Enrollees{count}</Card.Text>
                <Card.Text>Seats{count}</Card.Text>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>

		

	)
}

CourseCard.propTypes={
	// The shape method is used to check if a prop object conforms to a specific shape
	course:PropTypes.shape({
		// Defines the properties and their expected types
	name: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
	price: PropTypes.number.isRequired
	})
}