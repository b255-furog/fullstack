
import{Fragment, useEffect, useState} from 'react'
import CourseCard from '../components/CourseCard'
// import CoursesData from '../data/CoursesData'

export default function Courses(){
	// checks to see if the mock datya was captured
	/*console.log(CoursesData)
	console.log(CoursesData[0])
*/
	// The "course " in the CourseCard component is caled "prop" which is shorthand for "property" since componewnts are considered as objects in reactJS
	// the curly braces{} are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values
	// We can pass information from one compoinent to another using props. this is referred to as props drilling


	const [ courses, setCourses] = useState([])

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data => {
			console.log(data)

	setCourses(data.map(course=>{
		return(
			<CourseCard key={course.id}courseProp={course}/>
	
				)
			}))
		});
	}, [])

	
	return(
		<Fragment>
		{courses}
		</Fragment>

	)
}