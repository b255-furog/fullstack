
import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import{Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(props){

    // Allows us to consume the User context and its properties to use for user validation

const{user, setUser} = useContext(UserContext)

	// State hooks to store the value of the input fields


const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
// State to determine whether submit button is enabled or not
const [isActive, setIsActive] = useState('');

console.log(email)
console.log(password)
// Function to simulate user registration
function authenticate(e){
    // Prevent page redirection via form submission
    e.preventDefault()
    // Process a fecth request to the corresponding backend API 
    // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
    // The fetch request will communicate with our backend application providing it with a stringified JSON
    fetch('http://localhost:4000/users/login', {
        method: 'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body:JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res=>res.json())
    .then(data=>{
        console.log(data);

        // If no user information is found, the 'access' property will not be available and will return undefined
        if(typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access);
            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Zuitt!"
            });
        }else{
            Swal.fire({
                title: "Authentication failed",
                icon: "error",
                text: "Check your login details and try again"

            })
        }
    })


    
    // Set the email of the authenticated user in the local storage
    // Syntax

    // localStorage.setItem('email',email)

    // Set the global user state to have properties obtained from the local storage
    // Though access to the user information can be done via the local storage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing
    // When state change components are rerenderd and the AppNavBar component will be updated based on the user credentials

    /*setUser({
        email: localStorage.getItem('email')
    })
*/
    // Clear input fields
    setEmail('')
    setPassword('')

    alert('User successfully login')
}


const retrieveUserDetails = (token) =>{

    // The token will be sent as part of the requests header information
    fetch('http://localhost:4000/users/details',{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })

    .then(res=>res.json())
    .then(data=>{
        console.log(data)

        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        })
    })
}

useEffect(()=>{

// Validation to enable submit button when all fields are populated and both passwords match
if(email !=='' && password !== ''){
    setIsActive(true)
}else{
    setIsActive(false)
}
},[email,password])

        return(
            (user.id !== null)?
            <Navigate to ="/courses"/>
            :
    		<Form onSubmit ={(e)=>authenticate(e)}>
                <h2 class="">Login</h2>

                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value ={email}
                            onChange={e=> setEmail(e.target.value)} required
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value ={password}
                            onChange={e=> setPassword(e.target.value)} required 
                            required
                        />
                    </Form.Group>

                    {isActive ?

                    <Button variant="success" type="Login" id="loginBtn">
                        Login
                    </Button>
                    :
                     <Button variant="success" type="Login" id="loginBtn" disabled>
                        Login
                    </Button>
                }
          	</Form>

	)
}
/*
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
  const { user, setUser } = useContext(UserContext);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  const handleEmailValidation = async () => {
    const res = await fetch(`https://example.com/api/check-email?email=${email}`);
    const data = await res.json();
    if (data.emailExists) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Email already exists in the database. Please use a different email address.',
      });
      return false;
    }
    return true;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const isEmailValid = await handleEmailValidation();

    if (isEmailValid) {
      localStorage.setItem('email', email);

      setUser({
        email: localStorage.getItem('email'),
      });

      setEmail('');
      setPassword1('');
      setPassword2('');

      Swal.fire({
        icon: 'success',
        title: 'Registration Successful!',
        text: 'Thank you for registering.',
      });
    }
  };

  useEffect(() => {
    if (firstName && lastName && email && mobile && password1 && password2) {
      if (password1 === password2) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobile, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
      </Form.Group>

      <Form.Group controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
      </Form.Group>

      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
        <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
      </Form.Group>

      <Form.Group controlId="userMobile">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control type="tel" placeholder="Enter mobile number" value={mobile} onChange={(e) => setMobile(e.target.value.replace(/[^0-9]/g, '').slice(0, 11))}






*/