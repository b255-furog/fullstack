import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'



// Create root-assigns the element to be managed by React with its virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));


// render()-displays the react elements/components into the roots
// app components is our mother component, this is the component we use entry point and where we can render all other components
// <React.StrictMode>- compnents from the React thaht manges future or possible conflicts. It allows us extend or expand certain error messages

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*
const name = 'John Smith';

const user ={
  firstName: 'Jane',
  LastName: 'Smith'
}

function formatName(user){
  return user.firstName + ' ' + user.LastName;
}
const element = <h1>Hello,{formatName(user)}</h1>

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(element)
*/
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

